class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    get age() {
        return this._age;
    }

    set age(age) {
        this._age = age;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

    get lang() {
        return this._lang;
    }

    set lang(lang) {
        this._lang = lang;
    }
}

const programmer1 = new Programmer('Dima', 21, 5000, ['JavaScript', 'Python']);
const programmer2 = new Programmer('Mike', 25, 6000, ['Java', 'C++']);
const programmer3 = new Programmer('Anton', 19, 4500, ['Ruby', 'PHP']);

console.log(programmer1);
console.log(`Name: ${programmer1.name}, Age: ${programmer1.age}, Salary: ${programmer1.salary}, Languages: ${programmer1.lang}`);

console.log(programmer2);
console.log(`Name: ${programmer2.name}, Age: ${programmer2.age}, Salary: ${programmer2.salary}, Languages: ${programmer2.lang}`);

console.log(programmer3);
console.log(`Name: ${programmer3.name}, Age: ${programmer3.age}, Salary: ${programmer3.salary}, Languages: ${programmer3.lang}`);
